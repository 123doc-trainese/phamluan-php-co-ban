<?php
    // các từ khóa không phân biệt chữ hoa chữ thường
    echo "Hello world <br>";
    ECho "Hello world <br>";
    // Tên biến phân biệt chữ hoa chữ thường
    $name = "Pham Luan";
    $age = 22;
    $agE = 24;
    echo $name ." ".$age."<br>";
    echo "variable age: $age. variable agE: $agE<br>";


    /*
     * Phạm vi biến
     * local
     * global
     * static
     */
    //biến khai báo global khi sử dụng trong một hàm phải thông qua từ khóa global
    // biến khai báo local không thể gọi bên ngoài hàm khai báo nó
    function getAge(){
        $local = "đây là biến local";
        global $age;
        echo "global variable age: $age <br>";
        echo "không sử dụng từ khóa global để gọi biến: $agE <br>";
        echo "Đây là giá trị biến local trong hàm khai báo: $local<br>";
    }
    getAge();
    echo "Đây là giá trị biến local ngoài hàm khai báo: $local <br>";  //không thể gọi được biến local

    //biến static không bị xóa sau khi thực thi;
    function staticVar(){
        static $x = 0;
        $y = 0;
        echo "biến static: x = $x <br>";
        echo "biến không sử dụng static: y = $y <br>";
        $x++; $y++;
    }
    staticVar();    //x=0, y=0;
    staticVar();    //x=1, y =0
    staticVar();    //x=2, y=0

    //Hằng số
    define("GREETING", "Đây là hằng số");
    echo GREETING ."<br>";
    //mảng cố định
    define("cars", [
        "Táo",
        "Cam",
        "Quýt"
    ]);
    echo cars[0];
?>

