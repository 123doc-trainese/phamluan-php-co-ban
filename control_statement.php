<?php
//    Câu lệnh if...else
    $number = 5;
    if($number <0){
        echo "Số âm <br>";
    }elseif($number <=5){
        echo "Số nguyên dương nhỏ hơn 5 <br>";
    }else{
        echo "Số lớn hơn 5 <br>";
    }
//    output Số nguyên dương nhỏ hơn 5

//  Switch statements
    $key = "a";
    switch ($key){
        case "a":
            echo "kí tự a <br>";
            break;
        case "b":
            echo "kí tự b <br>";
            break;
        case "c":
            echo "kí tự c <br>";
            break;
    }
//    output Kí tự a;

//  while statement
    $count=0;
    while($count <5){
        echo "Vi du while $count <br>";
        $count++;
    }   //output: 0 1 2 3 4

// do...while statement
    $do = 0;
    do{
        echo "Vi du do...while $do <br>";
        $do++;
    }while($do <5);
    //output 0 1 2 3 4

// for statement
    for($i = 0; $i<=2; $i++){
        echo "vi du vong for $i <br>";
    } //output 0 1 2

// foreach statement
    $persons = array("ho" => "nguyen", "ten" => "nam", "tuoi" => '22');
    echo "Vi du foreach: ";
    foreach ($persons as $attr => $val){
        echo "$attr $val, ";
    }   //output: ho nguyen, ten nam, tuoi 22,
    echo '<br>';

?>