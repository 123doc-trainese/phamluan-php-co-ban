<?php
    /*
     * PHP data types
     * Integer
     * String
     * Float
     * Boolean
     * Null
     * Array
     * Object
     * Resource
     */
    $integer = 19;
    $float = 5.5;
    var_dump($integer); echo "<br>";
    var_dump($float); echo "<br>";

    //arrays
    $cars = array("Volvo","BMW",12);
    var_dump($cars); echo "<br>";

    //Object
    class Person{
        public $name, $age;
        public function __construct($name, $age)
        {
            $this->name = $name;
            $this->age = $age;
        }
        public function infor(){
            echo "Name: $this->name, Age: $this->age <br>";
        }
    }
    $luan = new Person('Luan', "22");
    $luan->infor();
    $p = new Person("An", '10');
    $p->infor();

    // Null
    $null = NULL;
    var_dump($null);
?>
