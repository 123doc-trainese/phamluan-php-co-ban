<?php

// array()
    $arr1 = array("cam", "vang", "do");     //mảng chỉ mục số
    $arr2 = array("name"=>"luan", "age"=>22, "city"=>"Ha noi");     //mảng liên kết
    $arr3 = array(              //mảng 2 chiều
        array("luan", 22),
        array("nguyen", 34),
        array("pham", 13)
    );

//  array_column()
    $a = array(
        array(
            'id' => "id1",
            'first_name' => 'Peter',
            'last_name' => 'Griffin',
        ),
        array(
            'id' => "id002",
            'first_name' => 'Ben',
            'last_name' => 'Smith',
        ),
        array(
            'id' => "id0003",
            'first_name' => 'Joe',
            'last_name' => 'Doe',
        )
    );
    print_r(array_column($a, "first_name"));        //output: Array ( [0] => Peter [1] => Ben [2] => Joe )
    echo "<br>";
    print_r(array_column($a, "first_name", "id"));  //output: Array ( [id1] => Peter [id002] => Ben [id0003] => Joe )
    echo "<br>";

//  array_combine()  tạo mảng mới từ 2 mảng cho trước
    $keys = array("luan", "An", "Nam");
    $values = [22, 25, 30];
    print_r(array_combine($keys, $values));     //output: Array ( [luan] => 22 [An] => 25 [Nam] => 30 )
    echo "<br>";

//    array_count_values()  đếm các giá trị của mảng
    $count=array("A","Cat","Dog","A","Dog");
    print_r(array_count_values($count));    //output: Array ( [A] => 2 [Cat] => 1 [Dog] => 2 )
    echo "<br>";
//  array_diff() so sánh giá trị của mảng
    echo "Vi du array_diff(): <br>";
    $a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
    $a2=array("f"=>"black","g"=>"purple");
    $a3=array("a"=>"red","b"=>"black","h"=>"yellow");
    print_r(array_diff($a1, $a2, $a3));     //output: Array ( [b] => green [c] => blue )

//  array_intersect() so sánh giá trị của các mảng, trả về các giá trị giống nhau
    echo "<br> Vi du array_intersect(): <br>";
    $intersect1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
    $intersect2=array("e"=>"red","f"=>"black");
    $intersect3=array("a"=>"red","h"=>"yellow");
    print_r(array_intersect($intersect1, $intersect2, $intersect3));    //output: Array ( [a] => red )

//  array_multisort()
    echo "<br> Vi du array_multisort(): <br>";
    $multisort1=array("Dc","C", "A", "Z");
    $multisort2=array("Fi","Mis", "ga", "na");
    array_multisort($multisort1,$multisort2);
    print_r($multisort1);       //output: Array ( [0] => A [1] => C [2] => Dc [3] => Z )
    echo "<br>";
    print_r($multisort2);       // output: Array ( [0] => ga [1] => Mis [2] => Fi [3] => na )
    echo "<br>";
//  sort()
    $cars=array("Volvo","BMW","Toyota");
    sort($cars);
    foreach ($cars as $car){
        echo "$car ";
    }       //output: BMW Toyota Volvo
    echo "<br>";

//  asort()
    $age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
    arsort($age);

    foreach($age as $x=>$x_value)
    {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }
    //output: Key=Joe, Value=43
    //Key=Ben, Value=37
    //Key=Peter, Value=35

//  krsort() sắp xếp mảng kếp hợp giảm dần theo khóa
    echo "krsort(): <br>";
    $age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
    krsort($age);
    foreach($age as $x=>$x_value)
    {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }
    //output: Key=Peter, Value=35
    //Key=Joe, Value=43
    //Key=Ben, Value=37

//  natsort()
    $temp_files = array("Temp15.txt","temp10.txt", "temp1.txt","temp22.txt","temp2.txt");
    natsort($temp_files);
    echo "Su dung natsort(): <br>";
    print_r($temp_files);

    echo "<br> su dung sort(): <br>";
    sort($temp_files);
    print_r($temp_files);

    echo "<br> su dung natcasesort(): <br>";
    natcasesort($temp_files);
    print_r($temp_files);
    //output: Su dung natsort():
    //Array ( [0] => Temp15.txt [2] => temp1.txt [4] => temp2.txt [1] => temp10.txt [3] => temp22.txt )
    //su dung sort():
    //Array ( [0] => Temp15.txt [1] => temp1.txt [2] => temp10.txt [3] => temp2.txt [4] => temp22.txt )
    //su dung natcasesort():
    //Array ( [1] => temp1.txt [3] => temp2.txt [2] => temp10.txt [0] => Temp15.txt [4] => temp22.txt )
?>