<?php
//    chunk_split() chia một chuỗi thành các thành chuỗi các thành phần nhỏ hơn
    $str_chunk = "Pham Van Luan";
    echo "Vi du chunk_split(): ".chunk_split($str_chunk, 4, "&")."<br>";    //output: Pham& Van& Luan&n&
    echo $str_chunk."<br>";

//    explode() cắt một chuỗi thành một mảng
    $str_explode = "mot, hai, ba, bon nam sau";
    echo 'su dụng limmit = 0: ';
    print_r(explode(',', $str_explode, 0));     //output Array ( [0] => mot, hai, ba, bon nam sau )
    echo "<br> Su dụng limit > 0: ";
    print_r(explode(',', $str_explode, 2));     //output Array ( [0] => mot [1] => hai, ba, bon nam sau )
    echo "<br> Su dung limit < 0: ";
    print_r(explode(',', $str_explode, -2));    //output Array ( [0] => mot [1] => hai )
    echo '<br>';
// implode() & join() chuyển một mảng thành một chuỗi
    $arr_implode = ["Pham", "Van", "Luan"];
    echo "Vi du implode(): ".implode($arr_implode)."<br>";      //output: PhamVanLuan
    echo implode(" ", $arr_implode)."<br>";            //output: Pham Van Luan
    echo "Vi du join(): ".join(" ", $arr_implode).'<br>';   //output: Pham Van Luan

//  printf() xuất ra một chuỗi được định dạng
    $age = 22;
    printf("Vi du printf(): tuổi = %u <br>", $age);     //output: tuổi = 22
    printf("So thu 1: %1\$u, so thu 2: %1\$u  <br>", $age);     //output: So thu 1: 22, so thu 2: 22

// spritf()
    $number = 123;
    $txt = sprintf("Vi du sprintf(): %f <br><br>",$number);
    echo $txt;

//  Uppercase & lowercase
    echo "Vi du text-transform: <br>";
    echo lcfirst("Pham Van Luan <br>");  //output: pham Van Luan
    echo ucfirst("pham van luan <br>");     //output: Pham van luan
    echo ucwords("pham van luan <br>");     //output: Pham Van Luan
    echo strtolower("Pham VAN LuAn <br>");  //output: pham van luan
    echo strtoupper("pham van luan <br>");  //output: PHAM VAN LUAN

// parse_str() chuyển một chuỗi truy vấn thành các biến
    parse_str("name=Peter&age=43", $parse);
    print_r($parse);    //output: Array ( [name] => Peter [age] => 43 )
    echo "<br>";
//  similar_text() trả về độ giống nhau của 2 chuỗi
    echo similar_text("Pham van", "pham luan", $percent)."<br>";    //ouput: 6
    printf("Phần trăm: %.3f <br>", $percent);     //output: Phần trăm: 70.588

//  str_ireplace()
    echo "vi du str_replace(): <br>";
    echo str_ireplace("VAN", 'Luan', 'Pham Van Luan')."<br>";      //output: Pham Luan Luan
    echo str_replace("Luan", "Van", "Pham Van LUAN Luan", $count)."<br>";  //output: Pham Van LUAN Van
    echo "So lan thay the: $count <br>";    //output: 1

// str_repeat()
    echo "Vi du str_repeat(): <br>";
    echo str_repeat("lap lai ", 3)."<br>";   //output: lap lai lap lai lap lai

// str_split()
    echo "<br>Vi du str_split: <br>";
    print_r(str_split("Pham Van Luan", 3));     //output: Array ( [0] => Pha [1] => m V [2] => an [3] => Lua [4] => n )
    echo "<br>";
// str_word_count()
    echo "Vi du str_world_count(): <br>";
    print_r(str_word_count("Pham Van Luan"));
    echo "<br>";
    print_r(str_word_count("Hello world!",1));
    echo "<br>";
    print_r(str_word_count("Pham Van Luan", 2));
    echo "<br>";

// strcasecmp() và strncasecmp() so sánh 2 chuỗi
    echo "Vi du so sanh chuoi: <br>";
    echo strcasecmp("LUAN", "Luan");    //output: 0
    echo "<br>";
    echo strncasecmp("Pham van luan", "pham luan", 4);  //ouput: 0
    echo "<br>";
    echo strncasecmp("Pham van luan", "pham luan", 6);  //ouput: 10
    echo "<br>";

//  strrev()
    echo "Đảo ngược chuỗi: <br>";
    echo strrev("Pham Van Luan");   //output: nauL naV mahP
    echo "<br>";

//    substr()
    echo "Cắt chuỗi: <br>";
    $str_sub = "Pham van Luan";
    echo substr($str_sub, 9)."<br>";   //output: Luan
    echo substr($str_sub, -4)."<br>";  //output: Luan
    echo substr($str_sub, 5, 3)."<br>"; //output: van
    echo substr($str_sub, 5, -5)."<br>"; //output: van

//  substr_replace()
    $str = "Pham Pham Pham";
    echo "thay thế chuỗi: <br>";
    echo substr_replace($str, "Van Luan", 5)."<br>";   //output: Pham Van Luan
    echo substr_replace($str, "Van Luan", 5, 2)."<br>";
    //output: Pham Van Luanam Pham  (vì chỉ thay thế 2 kí tự là Ph từ vị trí số 5)
    echo substr_replace($str, "Van Luan", 5, -2)."<br>";
    //output: Pham Van Luanam   (vì để lại 2 kí tự cuối cùng là am)
    echo substr_replace($str, "Van Luan ", -4, 0)."<br>";
    //output: Pham Pham Van Luan Pham   (chèn vào chuỗi tại vị trí 4 tính từ cuối chuỗi)
?>