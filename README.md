# PhamLuan Php Cơ bản



***PHP cơ bản***

- Tổng quan về Php
- Thực hiện bởi [Luận Phạm](https://facebook.com/phluann)

## Liên kết
- [PHP the right way](https://phptherightway.com/)
- [W3Schools PHP Tutorial](https://www.w3schools.com/php/)
- [Learn PHP](https://github.com/odan/learn-php)

## Kiến thức nắm được

***PHP introduction***
- PHP là từ viết tắt của "PHP: Hypertext Preprocessor"
- PHP là một ngôn ngữ kịch bản mã nguồn mở, được sử dụng rộng rãi
- Các tệp PHP có thể chứa văn bản, HTML, CSS, JavaScript và mã PHP
- Mã PHP được thực thi trên máy chủ và kết quả được trả về trình duyệt dưới dạng HTML thuần túy
- Tệp PHP có phần mở rộng " .php".
- Chạy code php ử dụng terminal: ```php -S localhost:5500```

***PHP syntax***
- Một tập lệnh php có thể đặt ở bất kỳ đâu trong tài liệu
- Một tập lệnh php bắt đầu bằng ```<?php``` và kết thúc bằng ```?>```
```
<?php
    //php code here
?>
```
- Trong PHP, các từ khóa, các class, hàm không phân biệt chữ hoa chữ thường
- Tên biến trong PHP phân biệt chữ hoa chữ thường

***PHP data types***
- PHP hỗ trợ các kiểu dữ liệu: ```String, Integer, Float, Boolean, Array, Object, Null, Resource```
- Để kiểm tra một biến php thuộc kiểu dữ liệu nào sử dụng: ```var_dump($variable);```

***Variables***
- Một biến bắt đầu bằng từ khóa `$`, theo sau là tên biến.
- Tên biến phải bắt đầu bằng một chữ cái hoặc ký tự gạch dưới
- Tên biến không thể bắt đầu bằng số
- Tên biến chỉ có thể chứa các ký tự chữ-số và dấu gạch dưới (Az, 0-9 và _)
- Tên biến có phân biệt chữ hoa chữ thường (`$age` và `$AGE` là hai biến khác nhau)
- Phạm vi biến: local, global, static
- biến khai báo global khi sử dụng trong một hàm phải thông qua từ khóa global
    ```
    $x = 5;
    $y = 10;
    function myTest() {
        global $x, $y; 
        $y = $x + $y;
    }
    myTest();
    echo $y; // output 15
  ```
- biến khai báo local không thể gọi bên ngoài hàm khai báo nó
- biến static không bị xóa sau khi thực thi, khai báo thông qua từ khóa `static`, ví dụ
    ```static $age;```
```
    function staticVar(){
        static $x = 0;
        $y = 0;
        echo "biến static: x = $x <br>";
        echo "biến không sử dụng static: y = $y <br>";
        $x++; $y++;
    }
    staticVar();    //x=0, y=0;
    staticVar();    //x=1, y =0
    staticVar();    //x=2, y=0
```

***Constants***
- hằng cũng giống như biến, tuy nhiên khi đã định nghĩa thì không thể thay đổi hoặc định nghĩa lại
- Tên hằng hợp lệ bắt đầu bằng một chữ cái hoặc dấu gạch dưới (không có dấu `$` trước tên hằng)
- Hằng số có thể sử dụng trên phạm vi toàn cục
- Định nghĩa một hằng số thông qua hàm `define(name, value, case-insensitive)`
  - name: chỉ định tên hằng số
  - value: chỉ định giá trị cho hằng số
  - case-insensitive: xác định hằng số có phân biệt chữ hoa chữ thường hay không, mặc định là 
  ```
    define("GREETING", "Welcome to W3Schools.com!"); //phân biệt chữ hoa chữ thường
    define("GREETING", "Welcome to W3Schools.com!", true); //không phân biệt chữ hoa chữ thường
  ```
### Control statements
***if...else***
- `if` thực thi một số mã nếu một điều kiện là đúng
- `if...else` thực thi một số lệnh nếu một điều kiện là đúng và một lệnh khác nếu điều kiện đó là sai
- `if...elseif...else` thực thi các  khác nhau cho nhiều hơn hai điều kiện
  ```
    $number = 5;
    if($number <0){
        echo "Số âm <br>";
    }elseif($number <=5){
        echo "Số nguyên dương nhỏ hơn 5 <br>";
    }else{
        echo "Số lớn hơn 5 <br>";
    }
  ```
***Switch***
  - `switch` chọn một trong nhiều khối mã sẽ được thực thi
  ```
  switch (n) {
    case label1:
      code to be executed if n=label1;
      break;
    case label2:
      code to be executed if n=label2;
      break;
      ...
    default:
      code to be executed if n is different from all labels;
  }
  ```

***Loop***
- trong php có các vòng lặp:
  - `while`lặp lại một khối code khi điều kiện được chỉ ra là đúng
  ```
    $count=0;
    while($count <5){
        echo "Vi du while $count <br>";
        $count++;
    }   //output: 0 1 2 3 4
  ```
  - `do...while` đoạn code được thực thi ít nhất là một lần, sau đó lặp lại nếu điều kiện được chỉ ra là đúng
  ```
    $do = 0;
    do{
        echo "Vi du do...while $do <br>";
        $do++;
    }while($do <5);
    //output 0 1 2 3 4
  ```
  - `for` thực thi một đoạn code theo số lần nhất định
  ```
    for($i = 0; $i<=2; $i++){
        echo "vi du vong for $i <br>";
    } //output 0 1 2
  ```
  - `foreach` lặp qua đoạn code cho mỗi phần tử trong mảng
  ```
    $persons = array("ho" => "nguyen", "ten" => "nam", "tuoi" => '22');
    echo "Vi du foreach: ";
    foreach ($persons as $attr => $val){
        echo "$attr $val, ";
    }   //output: ho nguyen, ten nam, tuoi 22,
  ```
### String functions

- `printf()`
  - hàm `printf` xuất ra một chuỗi được định dạng
  - cú pháp `printf(format, arg1, arg2, arg++)`
    - `format` chỉ định chuỗi và cách định dạng các biến trong đó
    - `arg1` đối số sẽ được truyền vào vị trí dấu `%` đầu tiên
    - `arg2` đối số sẽ được truyền vào vị trí dấy `%` thứ 2
    - `arg++` các đối số tiếp theo sẽ được truyền vào vị trí dấu `%` tương ứng
  ```
    $age = 22;
    printf("Vi du printf(): tuổi = %u <br>", $age);     //output: tuổi = 22
    printf("So thu 1: %1\$u, so thu 2: %1\$u", $age);     //output: So thu 1: 22, so thu 2: 22
  ```
- `sprintf()`: tác dụng tương tự như printf() nhưng không xuất ra màn hình mà cần lưu giá trị vào một biến khác
  ```
    $number = 123;
    $txt = sprintf("%f",$number);
    echo $txt;
  ```
- hàm `chunk_split()`
  - hàm `chunk_split()` dùng để chia một chuỗi các phần nhỏ hơn (không làm thay đổi chuỗi gốc)
  - cú pháp `chunk_split(string, length, end)`
    - `string` chuỗi cần tách (bắt buộc)
    - `length` xác định độ dài các chuỗi con, mặc định là 76 (không bắt buộc)
    - `end` chuỗi sẽ được thêm vào cuối mỗi chuỗi con, mặc định là `\r\n` (không bắt buộc)
  ```
    $str_chunk = "Pham Van Luan";
    echo chunk_split($str_chunk, 4, "&")."<br>";    //output: Pham& Van& Luan&n&
  ```

- hàm `explode()`
  - hàm `explode()` được sử dụng để cắt một chuỗi thành mảng
  - cú pháp `explode(seperator, string, limit)`
    - `seperator` chỉ định vị trí ngắt chuỗi (bắt buộc)
    - `string` chuỗi cần tách (bắt buộc)
    - `limit` chỉ định số phần tử mảng được trả về (không bắt buộc)
      -  `limit >0` trả về mảng với tối đa các phần tử trong giới hạn
      - `limit <0` trả về một mảng ngoại trừ các phần tử từ cuối cùng đến limit
      - `limit = 0` trả về mảng với một phần tử
  ```
    $str_explode = "mot, hai, ba, bon nam sau";
    echo 'su dụng limmit = 0: ';
    print_r(explode(',', $str_explode, 0));     //output Array ( [0] => mot, hai, ba, bon nam sau )
    
    echo "<br> Su dụng limit > 0: ";
    print_r(explode(',', $str_explode, 2));     //output Array ( [0] => mot [1] => hai, ba, bon nam sau )
    
    echo "<br> Su dung limit < 0: ";
    print_r(explode(',', $str_explode, -2));    //output Array ( [0] => mot [1] => hai )
  ```

- hàm `str_split()`
  - dùng để chia một chuỗi thành một mảng
  - cú pháp `str_split(string, length)`
    - `string` chuỗi cần tách (bắt buộc)
    - `length` độ dài mỗi phần tử mảng (tùy chọn, mặc định là 1)
  ```
    echo "<br>Vi du str_split: <br>";
    print_r(str_split("Pham Van Luan", 3));     
    //output: Array ( [0] => Pha [1] => m V [2] => an [3] => Lua [4] => n )
  ```
  
- hàm `implode()` và `join()`
  - hàm `implode()` và `join()` cùng được dùng để trả về một chuỗi từ các phần tử của một mảng
  - cú pháp: `implode(separator, array)` `join(separator, array)`
    - `separator` chỉ ra phần tử được đặt giữa các phần tử của mảng, mặc định là chuỗi rỗng (tùy chọn)
    - `array` mảng đang cần được chuyển đổi thành chuỗi (bắt buộc)
  ```
    $arr_implode = ["Pham", "Van", "Luan"];
    echo implode($arr_implode)."<br>";      //output: PhamVanLuan
    echo implode(" ", $arr_implode)."<br>";            //output: Pham Van Luan
    echo join(" ", $arr_implode).'<br>';   //output: Pham Van Luan
  ```

- Uppercase & lowercase
  - `lcfirst()` chuyển đổi kí tự đầu tiên của một chuỗi thành chữ thường
  
    ```echo lcfirst("Pham Van Luan <br>");  //output: pham Van Luan```
  - `ucfirst()` chuyển đổi kí tự đầu tiên của một chuỗi thành chữ hoa
  
    ```echo ucfirst("pham van luan <br>");      //output: Pham van luan```
  - `ucwords()` chuyển kí tự đầu tiên của mỗi phần tử trong một chuỗi thành chữ hoa

    ```echo ucwords("pham van luan <br>");     //output: Pham Van Luan```
  - `strtolower()` chuyển đổi một chuỗi thành chữ thường

    ```echo strtolower("Pham VAN LuAn <br>");  //output: pham van luan```
  - `strtoupper()` chuyển đổi một chuỗi thành chữ hoa
  
    ```echo strtoupper("pham van luan <br>");  //output: PHAM VAN LUAN```
- hàm `parse_str()`
  - hàm `parse_str()` phân tích chuỗi truy vấn thành các biến
  - cú pháp `parse_str(string, array)`
    - `string` chuỗi cần chuyển đổi
    - `array` tên mảng sẽ lưu trữ các biến (tùy chọn)
  ```
    parse_str("name=Peter&age=43", $parse);
    print_r($parse);    //output: Array ( [name] => Peter [age] => 43 )
  ```
  
- hàm `similar_text()`
  - tính toán độ giống nhau của 2 chuỗi
  - cú pháp `similar_text(string1, string2, percent)`
    - `string1`, `string2` hai chuỗi cần được so sánh (bắt buộc)
    - `percent` tên biến để lưu trữ độ giống nhau theo phần `
  ```
    echo similar_text("Pham van", "pham luan", $percent)."<br>";    //ouput: 6
    printf("Phần trăm: %.3f <br>", $percent)      //output: Phần trăm: 70.588
  ```
  
- hàm `str_ireplace()` và `str_replace()`
  - thay thế một số kí tự bằng một số kí tự khác trong một chuỗi
  - `str_ireplace()` không phân biệt chữ hoa chữ thường
  - `str_replace()` phân biệt chữ hoa chữ thường
  - cú pháp `str_ireplace(find, replace, string, count)`
    - `find` chuỗi kí tự cần tìm (bắt buộc)
    - `replace` chuỗi kí tự thay thế (bắt buộc)
    - `string` chuỗi cũ (bắt buộc)
    - `count` biến để đếm số lần thay thế (tùy chọn)
  
  ```
    echo str_ireplace("VAN", 'Luan', 'Pham Van Luan')."<br>";      //output: Pham Luan Luan
    echo str_replace("Luan", "Van", "Pham Van LUAN Luan", $count)."<br>";  //output: Pham Van LUAN Van
    echo "So lan thay the: $count <br>";    //output: 1
  ```
  
- hàm `str_repeat()` 
  - lặp lại một chuỗi theo số lần được chỉ định
  - cú pháp `str_repeat(string,repeat)`
    - `string` chuỗi cần lặp lại
    - `repeat` số lần chuỗi sẽ được lặp lại (>=0)
  ```
    echo "Vi du str_repeat(): <br>";
    echo str_repeat("lap lai ", 3)."<br>";   //output: lap lai lap lai lap lai
  ```
  
- hàm `str_word_count()`
  - dùng để đếm số từ trong một chuỗi
  - cú pháp `str_word_count(string,return,char)`
    - `string` chuỗi cần kiểm tra (bắt buộc)
    - `return` giá trị trả về (tùy chọn)
      - `0` mặc định, trả về số lượng từ được tìm thấy
      - `1` trả về mảng chứa các từ trong chuỗi
      - `2` trả về một mảng trong đó khóa là vị trí của từ trong chuỗi, giá trị là từ trong chuỗi
    - `char` chỉ định các kí tự đặc biệt được coi là từ (tùy chọn)
    ```
    print_r(str_word_count("Pham Van Luan"));   //output: 3
    echo "<br>";
    print_r(str_word_count("Hello world!",1));  //output: Array ( [0] => Hello [1] => world )
    echo "<br>";
    print_r(str_word_count("Pham Van Luan", 2));    //output: Array ( [0] => Pham [5] => Van [9] => Luan )
    echo "<br>";
    ```
    
- hàm `strcasecmp()` và `strncasecmp()`
  - được sử dụng để so sánh 2 chuỗi, không phân biệt chữ hoa chữ thường
  - cú pháp `strcasecmp(string1,string2)`, `strncasecmp(string1,string2,length)`
    - `string1`, `string2` hai chuỗi cần so sánh
    - `length` số kí tự dùng để so sánh trong mỗi chuỗi
  - giá trị trả về:
    - `0` nếu hai chuỗi bằng nhau
    - `<0` nếu `string1` nhỏ hơn `string2`
    - `>0` nếu `string` lớn hơn `string2`
  ```
    cho strcasecmp("LUAN", "Luan");    //output: 0
    echo "<br>";
    echo strncasecmp("Pham van luan", "pham luan", 4);  //ouput: 0
    echo "<br>";
    echo strncasecmp("Pham van luan", "pham luan", 6);  //ouput: 10
    echo "<br>";
  ```
  > Hàm `strcmp()` và `strncmp()` cũng được sử dụng tương tự nhưng có phân biệt chữ hoa chữ thường

- Hàm `strlen()`
  - cú pháp `strlen(string)`
    - `string` chuỗi cần kiểm tra
    - trả về độ dài của chuỗi
  ```
  echo strlen("Pham van luan");   //ouput: 13
  ```
  
- Hàm `strrev()`
  - Sử dụng để đảo ngược một chuỗi
  - cú pháp `strrev(string)`
    - `string` chuỗi cần được đảo ngược
  ```
  echo strrev("Pham Van Luan");   //output: nauL naV mahP
  ```
  
- hàm `substr()` 
  - trả về một phần của chuỗi
  - cú pháp `substr(string,start,length)`
    - `string` chuỗi ban đầu
    - `start` vị trí bắt đầu cắt chuỗi
      - một số `>0` bắt đầu từ vị trí chỉ định tính từ đầu chuỗi
      - một số `<0` bắt đầu từ vị trí chỉ định tính từ cuối chuỗi
      - `0` bắt đầu từ kí tự đầu tiên trong chuỗi
    - `length` độ dài của chuỗi được trả về (mặc định là lấy đến cuối chuỗi)
      - `0, Null, false` trả về một chuỗi 
    ```
    $str_sub = "Pham van Luan";
    echo substr($str_sub, 9)."<br>";   //output: Luan
    echo substr($str_sub, -4)."<br>";  //output: Luan
    echo substr($str_sub, 5, 3)."<br>"; //output: van
    echo substr($str_sub, 5, -5)."<br>"; //output: van
    ```
- Hàm `substr_replace()`
  - thay thế một phần của chuỗi bằng một chuỗi khác
  - cú pháp `substr_replace(string,replacement,start,length)`
    - `string` chuỗi ban đầu
    - `replacement` chuỗi thay thế
    - `start` vị trí bắt đầu thay thế
      - một số `>0` bắt đầu từ vị trí chỉ định tính từ đầu chuỗi
      - một số `<0` bắt đầu từ vị trí chỉ định tính từ cuối chuỗi
      - `0` bắt đầu từ kí tự đầu tiên trong chuỗi
    - `length` chỉ định số lượng kí tự được thay thế (tùy chọn, mặc định là bằng độ dài chuỗi)
      - một số `>0` số lượng kí tự được thay thế
      - một số `<0` số kí tự không bị thay thế tính từ cuối chuỗi
      - `0` chèn vào chuỗi cũ mà không thay thế
  ```
    $str = "Pham Pham Pham";
    echo "thay thế chuỗi: <br>";
    echo substr_replace($str, "Van Luan", 5)."<br>";   //output: Pham Van Luan
    echo substr_replace($str, "Van Luan", 5, 2)."<br>";
    //output: Pham Van Luanam Pham  (vì chỉ thay thế 2 kí tự là Ph từ vị trí số 5)
    echo substr_replace($str, "Van Luan", 5, -2)."<br>";
    //output: Pham Van Luanam   (vì để lại 2 kí tự cuối cùng là am)
    echo substr_replace($str, "Van Luan ", -4, 0)."<br>";
    //output: Pham Pham Van Luan Pham   (chèn vào chuỗi tại vị trí 4 tính từ cuối chuỗi)
  ```
  
### Array functions

- `array()`
  - được sử dụng để tạo một mảng
  - có 3 loại mảng:
    - mảng có chỉ mục số
    - mảng liên kết: mảng với chỉ mục được đặt tên
    - mảng đa chiều
  - cú pháp `array(value1, value2, value3, etc.)`, `array(key=>value,key=>value,key=>value,etc.)`
  ```
    $arr1 = array("cam", "vang", "do");     //mảng chỉ mục số
    $arr2 = array("name"=>"luan", "age"=>22, "city"=>"Ha noi");     //mảng liên kết
    $arr3 = array(              //mảng 2 chiều
        array("luan", 22),
        array("nguyen", 34),
        array("pham", 13)
    );
  ```
  
- hàm `array_column()`
  - trả về các giá trị từ một cột trong mảng
  - cú pháp `array_column(array, column_key, index_key)`
    - `array` mảng đầu vào (mảng đa chiều)
    - `column_key` khóa của cột cần lấy giá trị
    - `index_key` cột để làm chỉ mục/ khóa cho mảng được trả về (tùy chọn)
  ```
    $a = array(
        array(
            'id' => "id1",
            'first_name' => 'Peter',
            'last_name' => 'Griffin',
        ),
        array(
            'id' => "id002",
            'first_name' => 'Ben',
            'last_name' => 'Smith',
        ),
        array(
            'id' => "id0003",
            'first_name' => 'Joe',
            'last_name' => 'Doe',
        )
    );
    print_r(array_column($a, "first_name"));        //output: Array ( [0] => Peter [1] => Ben [2] => Joe )
    echo "<br>";
    print_r(array_column($a, "first_name", "id"));  //output: Array ( [id1] => Peter [id002] => Ben [id0003] => Joe )
    echo "<br>";
  ```
  
- Hàm `array_combine()`
  - tạo một mảng bằng cách sử dụng các phần tử từ 2 mảng khác, một mảng làm khóa và một mảng làm giá trị
  - cả 2 mảng phải có số phần tử bằng nhau
  - cú pháp `array_combine(keys, values)`
    - `keys` mảng để làm khóa
    - `values` mảng để làm giá trị
  ```
    $keys = array("luan", "An", "Nam");
    $values = [22, 25, 30];
    print_r(array_combine($keys, $values));     //output: Array ( [luan] => 22 [An] => 25 [Nam] => 30 )
    echo "<br>";
  ```
  
- Hàm `array_count_values()`
  - sử dụng để đếm các giá trị của một mảng
  - cú pháp `array_count_values(array)`
    - `array` mảng ban đầu
  ```
    $count=array("A","Cat","Dog","A","Dog");
    print_r(array_count_values($count));    //output: Array ( [A] => 2 [Cat] => 1 [Dog] => 2 )
  ```

#### So sánh

- Hàm `array_diff()`
  - so sánh giá trị của 2 hoặc nhiều mảng và trả về sự khác biệt
  - cú pháp `array_diff(array1, array2, array3, ...)`
    - `array1` mảng cần được so sánh (bắt buộc)
    - `array2` mảng so sánh (bắt buộc)
    - `array3, ...` mảng so sánh (tùy chọn)
  - trả về một mảng chứa các giá trị có ở array1 mà không có ở array2 hoặc array3, ...
  ```
    $a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
    $a2=array("f"=>"black","g"=>"purple");
    $a3=array("a"=>"red","b"=>"black","h"=>"yellow");
    print_r(array_diff($a1, $a2, $a3));     //output: Array ( [b] => green [c] => blue )
  ```
  - Tương tự array_diff(), các hàm: 
    - `array_diff_assoc()` so sánh khóa và giá trị của 2 hoặc nhiều mảng và trả về sự khác biệt
    - `array_diff_key()` so sánh khóa của 2 hoặc nhiều mảng và trả về sự khác biệt

- Hàm `array_intersect()`
  - so sánh giá trị của 2 hoặc nhiều mảng, trả về các giá trị giống nhau
  - cú pháp `array_intersect(array1, array2, array3, ...)`
    - `array1` mảng cần được so sáng (bắt buộc)
    - `array2` mảng so sánh (bắt buộc)
    - `array3, ...` mảng so sánh (tùy chọn)
  - trả về một mảng chứa các giá trị có trong array1, cũng có trong array2, array3, ...
  ```
    $intersect1=array("a"=>"red","b"=>"green","c"=>"black","d"=>"yellow");
    $intersect2=array("e"=>"red","f"=>"black");
    $intersect3=array("a"=>"red","h"=>"yellow");
    print_r(array_intersect($intersect1, $intersect2, $intersect3));    //output: Array ( [a] => red )
  ```
  - Tương tự array_intersect(), các hàm:
  - `array_intersect_assoc()` so sánh khóa và giá trị của các mảng, trả về mảng các giá trị giống nhau
  - `array_intersect_key()` so sánh khóa của các mảng, trả về mảng các giá trị giống nhau

#### sắp xếp

- Hàm `array_multisort()`
  - trả về một mảng đã được sắp xếp, có thể truyền vào một hoặc nhiều mảng
  - Hàm sẽ sắp xếp mảng đầu tiên, và các mảng khác theo sau, nếu hai hoặc nhiều giá trị giống nhau nó sẽ sắp xếp mảng tiếp theo
  - cú pháp `array_multisort(array1, sortorder, sorttype, array2, array3, ...)`
    - `array1` mảng ban đầu (bắt buộc)
    - `sortorder` thứ tự sắp xếp (tùy chọn)
      - `SORT_ASC` giá trị mặc định, sắp xếp theo thứ tự tăng dần (A-Z)
      - `SORT_DESC` sắp xếp theo thứ tự giảm dần (Z-A)
    - `sorttype` (tùy chọn)
      - `SORT_REGULAR` mặc đinh, so sánh các phần tử bình thường (theo chuẩn ASCII)
      - `SORT_NUMBERIC` so sánh các phần tử dưới dạng giá trị số
      - `SORT_STRING` so sánh các phần tử dưới dạng giá trị chuỗi
      - `SORT_LOCALE_STRING` so sánh các phần tử dưới dạng giá trị chuỗi dựa trên ngôn ngữ hiện tại
      - `SORT_NATURAL` so sánh các phần tử dưới dạng chuỗi bằng cách sử đụng thứ tự tự nhiên
      - `SORT_FLAG_CASE` có thể kết hợp với `SORT_STRING` hoặc `SORT_NATURAL` để sắp xếp các chuỗi không phân biệt chữ hoa thường
    - `array2, array3, ...` các mảng tiếp theo
  ```
    $multisort1=array("Dc","C", "A", "Z");
    $multisort2=array("Fi","Mis", "ga", "na");
    array_multisort($multisort1,$multisort2);
    print_r($multisort1);       //output: Array ( [0] => A [1] => C [2] => Dc [3] => Z )
    echo "<br>";
    print_r($multisort2);       // output: Array ( [0] => ga [1] => Mis [2] => Fi [3] => na )
  ```
- hàm `sort()`, `rsort()`
  - `sort()` sắp xếp một mảng chỉ mục theo thứ tự tăng dần
  - `rsort()` sắp xếp một mảng chỉ mục theo thứ tự giảm dần
  - cú pháp `sort(array, sorttype)`, `rsort(array, sorttype)`
    - `array` mảng cần sắp xếp
    - `sorttype` các so sánh các phần tử mảng (tùy chọn)
      - `0 = SORT_REGULAR` so sánh thông thường (mặc định)
      - `1 = SORT_NUMERIC` so sánh các phần tử bằng số
      - `2 = SORT_STRING` so sánh các phần tử dưới dạng chuỗi
      - `3 = SORT_LOCALE_STRING` so sánh các phần tử dưới dạng chuỗi, dựa trên ngôn ngữ hiện tại
      - `4 = SORT_NATURAL` so sánh các phần tử dưới dạng chuỗi, sử dụng thứ tự tự nhiên
    ```
    $cars=array("Volvo","BMW","Toyota");
    sort($cars);
    foreach ($cars as $car){
        echo "$car ";
    }       //output: BMW Toyota Volvo
    ```
    
- hàm `asort()`, `arsort()`
  - `asort()` sắp xếp một mảng kết hợp theo thứ tự tăng dần, theo giá trị
  - `arsort()` sắp xếp một mảng kết hợp theo thứ tự giảm dần, theo giá trị
  - cú pháp `asort(array, sorttype)`, `arsort(array, sorttype)`
    - `array` mảng cần sắp xếp
    - `sorttype` các so sánh các phần tử mảng (tùy chọn)
      - `0 = SORT_REGULAR` so sánh thông thường (mặc định)
      - `1 = SORT_NUMERIC` so sánh các phần tử bằng số
      - `2 = SORT_STRING` so sánh các phần tử dưới dạng chuỗi
      - `3 = SORT_LOCALE_STRING` so sánh các phần tử dưới dạng chuỗi, dựa trên ngôn ngữ hiện tại
      - `4 = SORT_NATURAL` so sánh các phần tử dưới dạng chuỗi, sử dụng thứ tự tự nhiên
    ```
    $age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
    arsort($age);

    foreach($age as $x=>$x_value)
    {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }
    //output: Key=Joe, Value=43
    //Key=Ben, Value=37
    //Key=Peter, Value=35
    ```
    
- hàm `ksort()`, `krsort()`
  - `ksort()` sắp xếp mảng kết hợp theo thứ tự tăng dần, theo khóa
  - `krsort()` sắp xếp mảng kết hợp theo thứ tự giảm dần, theo khóa
  - cú pháp `ksort(array, sorttype)`, `krsort(array, sorttype)`
    - `array` mảng cần sắp xếp
    - `sorttype` các so sánh các phần tử mảng (tùy chọn)
      - `0 = SORT_REGULAR` so sánh thông thường (mặc định)
      - `1 = SORT_NUMERIC` so sánh các phần tử bằng số
      - `2 = SORT_STRING` so sánh các phần tử dưới dạng chuỗi
      - `3 = SORT_LOCALE_STRING` so sánh các phần tử dưới dạng chuỗi, dựa trên ngôn ngữ hiện tại
      - `4 = SORT_NATURAL` so sánh các phần tử dưới dạng chuỗi, sử dụng thứ tự tự nhiên
    ```
    $age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
    krsort($age);
    foreach($age as $x=>$x_value)
    {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }
    //output: Key=Peter, Value=35
    //Key=Joe, Value=43
    //Key=Ben, Value=37
    ```
    
- hàm `natsort()`, `natcasesort()`: sắp xếp theo thuật toán tự nhiên (alphabet)
  - `natsort()` Các phần tử sẽ được sắp xếp dựa theo giá trị của phần tử dựa vào trật tự alphabet (có phân biệt chữ hoa chữ thường)
  - `natcasesort()` Các phần tử sẽ được sắp xếp dựa theo giá trị của phần tử dựa vào trật tự alphabet (không phân biệt hoa thường)
  - Trong một thuật toán tự nhiên, số 2 nhỏ hơn số 10. Trong máy tính sắp xếp, 10 nhỏ hơn 2, vì số đầu tiên trong "10" nhỏ hơn 2
  - cú pháp: `natsort(array)`, `natcasesort(array)`
  ```
    $temp_files = array("Temp15.txt","temp10.txt", "temp1.txt","temp22.txt","temp2.txt");
    natsort($temp_files);
    echo "Su dung natsort(): <br>";
    print_r($temp_files);

    echo "<br> su dung sort(): <br>";
    sort($temp_files);
    print_r($temp_files);

    echo "<br> su dung natcasesort(): <br>";
    natcasesort($temp_files);
    print_r($temp_files);
    //output: Su dung natsort():
    //Array ( [0] => Temp15.txt [2] => temp1.txt [4] => temp2.txt [1] => temp10.txt [3] => temp22.txt )
    //su dung sort():
    //Array ( [0] => Temp15.txt [1] => temp1.txt [2] => temp10.txt [3] => temp2.txt [4] => temp22.txt )
    //su dung natcasesort():
    //Array ( [1] => temp1.txt [3] => temp2.txt [2] => temp10.txt [0] => Temp15.txt [4] => temp22.txt )
  ```
  